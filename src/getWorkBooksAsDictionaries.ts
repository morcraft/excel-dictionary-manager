import WorkBookCollection from './WorkBookCollection'
import DictionaryCollection from './DictionaryCollection'
import getWorkBookAsDictionary from './getWorkBookAsDictionary'

export default function getWorkBooksAsDictionaries(workBooks: WorkBookCollection, keyIndex: number, valueIndex: number): DictionaryCollection{
    const map = {}
    Object.keys(workBooks).forEach(key => {
        map[key] = getWorkBookAsDictionary(workBooks[key], keyIndex, valueIndex)
    })
    return map
}