import Dictionary from './Dictionary'
import DictionaryCollection from './DictionaryCollection'
import excelExtensions from './excelExtensions'
import makeExcelTranslationFile from './makeExcelTranslationFile'
import getSpreadSheets from './getSpreadSheets'
import spreadSheetsToJson from './spreadSheetsToJson'
import temporaryFilesRegexp from './temporaryFilesRegexp'
import readExcelWorkBook from './readExcelWorkBook'
import { Workbook } from 'exceljs'
import WorkBookCollection from './WorkBookCollection'
import getWorkBookAsDictionary from './getWorkBookAsDictionary'
import getWorkBooksAsDictionaries from './getWorkBooksAsDictionaries'


export { excelExtensions, makeExcelTranslationFile, getSpreadSheets, spreadSheetsToJson, temporaryFilesRegexp }
export { readExcelWorkBook }
export { Workbook as WorkBook, WorkBookCollection, getWorkBookAsDictionary }
export { Dictionary, DictionaryCollection, getWorkBooksAsDictionaries }