import Excel = require('exceljs')
import getSpreadSheets from './getSpreadSheets'
import fs = require('fs')
import path = require('path')

export default function spreadSheetsToJson(originPath: string, targetPath: string, keyIndex: number, valueIndex: number){
	const spreadSheets = getSpreadSheets(originPath)
	
	spreadSheets.forEach(fileName => {
		const name = path.parse(fileName).name
		const workBook = new Excel.Workbook()
		workBook.xlsx.readFile(fileName)
		.then((data) => {
			const map = {}
			data.worksheets[0].eachRow(row => {
				const key = row.getCell(keyIndex).text
				const text = row.getCell(valueIndex).text
				map[key] = text
			})
			
			const string = JSON.stringify(map, null, '\t')
			fs.writeFileSync(path.join(targetPath, `${name}.json`), string)
		})
		.catch(error => {
			console.error(error)
		})
	})
}
