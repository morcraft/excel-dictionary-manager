import Excel = require('exceljs')
import Dictionary from './Dictionary'

export function addInfoRow(sheet: Excel.Worksheet, originLanguage: string, targetLanguage: string){
    return sheet.columns = [
        { header: 'ID', key: 'ID', width: 25, },
        { header: originLanguage, key: originLanguage, width: 120, },
        { header: targetLanguage, key: targetLanguage, width: 120, },
    ]
}

export function stylizeRow(row: Excel.Row){
    return row.eachCell(cell => {
        //cell.font.bold = true
        cell.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb:'FF609196', },
        }
    })
}

export default function makeExcelTranslationFile(dictionary: Dictionary, originLanguage: string, targetLanguage: string){
    const workBook = new Excel.Workbook()
    workBook.creator = 'Me!'
    workBook.lastModifiedBy = 'Also me!'
    workBook.created = new Date()
    workBook.properties.date1904 = true
    
    const sheet = workBook.addWorksheet(`${originLanguage} -> ${targetLanguage}`)
    addInfoRow(sheet, originLanguage, targetLanguage)
    stylizeRow(sheet.getRow(1))

    Object.keys(dictionary).sort().forEach(key => {
        sheet.addRow([null, key, dictionary[key]])
    })

    return workBook
}