import Excel = require('exceljs')
import temporaryFilesRegexp from './temporaryFilesRegexp'
import getFileExtension from './getFileExtension';
import excelExtensions from './excelExtensions';

export default function getExcelWorkBook(filePath: string){
    if(temporaryFilesRegexp.test(filePath))
        throw new Error(`Invalid file ${filePath} to use as spreadsheet. Is it temporary?`)

    const extension = getFileExtension(filePath)
    if(!excelExtensions.includes(extension))
        throw new Error(`Invalid file ${filePath}. It doesn't contain a valid spreadsheet extension.`)
	
    const workBook = new Excel.Workbook()
    return workBook.xlsx.readFile(filePath)
}
