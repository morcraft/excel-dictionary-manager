import path = require('path')
import fs = require('fs')
import excelExtensions from './excelExtensions'
import temporaryFilesRegexp from './temporaryFilesRegexp'

export default function getSpreadsheets(targetPath: string): string[]{
    const files: string[] = []
    fs.readdirSync(targetPath).forEach(fileName => {
        const extension = path.extname(fileName).split('.').pop().toLowerCase()
        
        if(temporaryFilesRegexp.test(fileName)) return true //ignore temporary files

        if(excelExtensions.includes(extension))
            files.push(path.join(__dirname, fileName))
    })

    return files
}