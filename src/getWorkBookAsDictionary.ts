import { WorkBook } from './index'
import Dictionary from './Dictionary'

export default function getWorkBookAsDictionary(workBook: WorkBook, keyIndex: number, valueIndex: number): Dictionary{
    const map = {}
    workBook.worksheets[0].eachRow(row => {
        const key = row.getCell(keyIndex).text
        const text = row.getCell(valueIndex).text
        map[key] = text
    })
    
    return map
}