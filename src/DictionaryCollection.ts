import Dictionary from './Dictionary'

export default interface DictionaryCollection{
    [s: string]: Dictionary
}