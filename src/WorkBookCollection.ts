import { WorkBook } from './index'

export default interface WorkBookCollection{
    [s: string]: WorkBook
}