"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var getWorkBookAsDictionary_1 = require("./getWorkBookAsDictionary");
function getWorkBooksAsDictionaries(workBooks, keyIndex, valueIndex) {
    var map = {};
    Object.keys(workBooks).forEach(function (key) {
        map[key] = getWorkBookAsDictionary_1.default(workBooks[key], keyIndex, valueIndex);
    });
    return map;
}
exports.default = getWorkBooksAsDictionaries;
