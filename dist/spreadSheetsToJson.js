"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Excel = require("exceljs");
var getSpreadSheets_1 = require("./getSpreadSheets");
var fs = require("fs");
var path = require("path");
function spreadSheetsToJson(originPath, targetPath, keyIndex, valueIndex) {
    var spreadSheets = getSpreadSheets_1.default(originPath);
    spreadSheets.forEach(function (fileName) {
        var name = path.parse(fileName).name;
        var workBook = new Excel.Workbook();
        workBook.xlsx.readFile(fileName)
            .then(function (data) {
            var map = {};
            data.worksheets[0].eachRow(function (row) {
                var key = row.getCell(keyIndex).text;
                var text = row.getCell(valueIndex).text;
                map[key] = text;
            });
            var string = JSON.stringify(map, null, '\t');
            fs.writeFileSync(path.join(targetPath, name + ".json"), string);
        })
            .catch(function (error) {
            console.error(error);
        });
    });
}
exports.default = spreadSheetsToJson;
