"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var fs = require("fs");
var excelExtensions_1 = require("./excelExtensions");
var temporaryFilesRegexp_1 = require("./temporaryFilesRegexp");
function getSpreadsheets(targetPath) {
    var files = [];
    fs.readdirSync(targetPath).forEach(function (fileName) {
        var extension = path.extname(fileName).split('.').pop().toLowerCase();
        if (temporaryFilesRegexp_1.default.test(fileName))
            return true; //ignore temporary files
        if (excelExtensions_1.default.includes(extension))
            files.push(path.join(__dirname, fileName));
    });
    return files;
}
exports.default = getSpreadsheets;
