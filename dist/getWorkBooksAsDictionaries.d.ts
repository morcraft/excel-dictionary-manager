import WorkBookCollection from './WorkBookCollection';
import DictionaryCollection from './DictionaryCollection';
export default function getWorkBooksAsDictionaries(workBooks: WorkBookCollection, keyIndex: number, valueIndex: number): DictionaryCollection;
