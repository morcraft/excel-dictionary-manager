"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getFileExtension(filePath) {
    var lastDotPos = filePath.lastIndexOf('.');
    if (lastDotPos < 0)
        return '';
    if (lastDotPos == filePath.length - 1)
        return '';
    return filePath.substr(lastDotPos + 1);
}
exports.default = getFileExtension;
