import { WorkBook } from './index';
import Dictionary from './Dictionary';
export default function getWorkBookAsDictionary(workBook: WorkBook, keyIndex: number, valueIndex: number): Dictionary;
