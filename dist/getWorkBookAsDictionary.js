"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getWorkBookAsDictionary(workBook, keyIndex, valueIndex) {
    var map = {};
    workBook.worksheets[0].eachRow(function (row) {
        var key = row.getCell(keyIndex).text;
        var text = row.getCell(valueIndex).text;
        map[key] = text;
    });
    return map;
}
exports.default = getWorkBookAsDictionary;
