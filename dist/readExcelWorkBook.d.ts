import Excel = require('exceljs');
export default function getExcelWorkBook(filePath: string): Promise<Excel.Workbook>;
