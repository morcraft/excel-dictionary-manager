import Excel = require('exceljs');
import Dictionary from './Dictionary';
export declare function addInfoRow(sheet: Excel.Worksheet, originLanguage: string, targetLanguage: string): {
    header: string;
    key: string;
    width: number;
}[];
export declare function stylizeRow(row: Excel.Row): void;
export default function makeExcelTranslationFile(dictionary: Dictionary, originLanguage: string, targetLanguage: string): Excel.Workbook;
