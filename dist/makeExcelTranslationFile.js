"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Excel = require("exceljs");
function addInfoRow(sheet, originLanguage, targetLanguage) {
    return sheet.columns = [
        { header: 'ID', key: 'ID', width: 25, },
        { header: originLanguage, key: originLanguage, width: 120, },
        { header: targetLanguage, key: targetLanguage, width: 120, },
    ];
}
exports.addInfoRow = addInfoRow;
function stylizeRow(row) {
    return row.eachCell(function (cell) {
        //cell.font.bold = true
        cell.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: 'FF609196', },
        };
    });
}
exports.stylizeRow = stylizeRow;
function makeExcelTranslationFile(dictionary, originLanguage, targetLanguage) {
    var workBook = new Excel.Workbook();
    workBook.creator = 'Me!';
    workBook.lastModifiedBy = 'Also me!';
    workBook.created = new Date();
    workBook.properties.date1904 = true;
    var sheet = workBook.addWorksheet(originLanguage + " -> " + targetLanguage);
    addInfoRow(sheet, originLanguage, targetLanguage);
    stylizeRow(sheet.getRow(1));
    Object.keys(dictionary).sort().forEach(function (key) {
        sheet.addRow([null, key, dictionary[key]]);
    });
    return workBook;
}
exports.default = makeExcelTranslationFile;
