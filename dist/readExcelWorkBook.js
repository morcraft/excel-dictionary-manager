"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Excel = require("exceljs");
var temporaryFilesRegexp_1 = require("./temporaryFilesRegexp");
var getFileExtension_1 = require("./getFileExtension");
var excelExtensions_1 = require("./excelExtensions");
function getExcelWorkBook(filePath) {
    if (temporaryFilesRegexp_1.default.test(filePath))
        throw new Error("Invalid file " + filePath + " to use as spreadsheet. Is it temporary?");
    var extension = getFileExtension_1.default(filePath);
    if (!excelExtensions_1.default.includes(extension))
        throw new Error("Invalid file " + filePath + ". It doesn't contain a valid spreadsheet extension.");
    var workBook = new Excel.Workbook();
    return workBook.xlsx.readFile(filePath);
}
exports.default = getExcelWorkBook;
